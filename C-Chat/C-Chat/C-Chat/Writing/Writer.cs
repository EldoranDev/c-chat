﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace C_Chat.Writing
{
    class Writer
    {
        public void Parse(string Stda, RichTextBox box)
        {
            string[] parts = Stda.Split('|');
            switch (parts.Length)
            {
                case 2:
                    Write(parts[0], parts[1], box);
                    break;
                default:
                    break;
            }
        }

        private void Write(string Name, string Text, RichTextBox box)
        {
            String text = "<" + Name + ">: " + Text + "\n";
            box.AppendText(text);
            box.SelectionStart = box.Text.Length;
            box.ScrollToCaret();
        }
    }
}
