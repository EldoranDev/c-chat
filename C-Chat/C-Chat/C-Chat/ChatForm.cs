﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using C_Chat.Writing;

namespace C_Chat
{
    public partial class ChatForm : Form
    {

        private Thread listenerThread;
        private Funktions.Broadcast Sender;
        private int mSecs = 0;
        private Writing.Writer myWriter = new Writer();
        private List<Thread> MyThreads = new List<Thread>();

        public ChatForm()
        {
            InitializeComponent();
            Sender = new Funktions.Broadcast();
        }

        private void JoinChat_Click(object sender, EventArgs e)
        {
            Funktions.NamenEingeben Name = new Funktions.NamenEingeben();
            Name.ShowDialog();
            listenerThread = new Thread(new ThreadStart(ThreadFunction));
            MyThreads.Add(listenerThread);
            listenerThread.Start();
            Sender.BroadCastSend(UnicodeEncoding.UTF8.GetBytes("System|" + Configs.User.Name + " joint the chat"));
            this.ToWrite.ReadOnly = false;
            this.JoinChat.Available = false;
            Counter.Start();
        }

        public void ThreadFunction()
        {

            Funktions.Listerner listenerObjekt = new Funktions.Listerner(this, WrittenText);
            listenerObjekt.run();


        }

        private void FToWrite(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                this.Senden();
            }
        }

        private void Senden()
        {
            if (Configs.User.PostCount < Configs.User.PostPerMinMax)
            {
                mSecs = 0;
                Byte[] bString = new byte[1024];
                if (!this.ToWrite.Text.Equals(""))
                {
                    bString = UnicodeEncoding.UTF8.GetBytes(Configs.User.Name + "|" + this.ToWrite.Text);
                    Sender.BroadCastSend(bString);
                    this.ToWrite.Text = "";
                }
                Configs.User.PostCount++;
            }
            else
            {
                Cantsend();
            }
        }

        private void Cantsend()
        {
            myWriter.Parse("system|You Cant Write at the Moment", this.WrittenText);
            this.ToWrite.Text = "";
        }

        private void FToWrite(object sender, EventArgs e)
        {
            this.Senden();
        }

        private void ChatForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Sender.BroadCastSend(ASCIIEncoding.ASCII.GetBytes("System|"+Configs.User.Name+" Left the Chat. "));
            Process pp = Process.GetCurrentProcess();
            pp.Kill();
        }

        private void Checker_Tick(object sender, EventArgs e)
        {
            if (mSecs > (Configs.User.MuteTime * 1000))
            {
                Configs.User.PostCount = 0;
                mSecs = 0;
            }
        }

        private void Counter_Tick(object sender, EventArgs e)
        {
            mSecs++;
        }
    }
}
