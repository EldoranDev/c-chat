﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



namespace C_Chat.Funktions
{

    public partial class NamenEingeben : Form
    {
        public NamenEingeben()
        {
            InitializeComponent();
            this.textBox1.Text = Configs.User.EnvName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            set();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                set();
            }
        }

        private void set()
        {
            if (!this.textBox1.Text.Equals(""))
            {
                if (!Funktions.MyRegEx.RegEx(this.textBox1.Text, ' ') & !Funktions.MyRegEx.RegEx(this.textBox1.Text, '|'))
                {
                   Configs.User.Name = this.textBox1.Text;
                   this.Close();
                }
                else
                {
                    MessageBox.Show("Bitte verwende keine Leerzeichen oder |  in deinem namen");
                }
            }
        }
    }
}
