﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace C_Chat.Funktions
{
    class Broadcast
    {
        public void BroadCastSend(byte[] data)
        {
            this.BroadCastSend(data, IPAddress.Broadcast, Configs.Broadcast.Port);
        }

        private void BroadCastSend(byte[] data, IPAddress ip, int Port)
        {
            Socket bcSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint iep1 = new IPEndPoint(ip, Port);
            bcSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            bcSocket.SendTo(data, iep1);

            bcSocket.Close();
        }
    }
}
