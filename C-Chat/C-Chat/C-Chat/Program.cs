﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace C_Chat
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Setting Configs
            Configs.User.EnvName = System.Environment.UserName.ToString();
            Configs.User.EnvOS = System.Environment.OSVersion;
            //
            //End of Configs
            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ChatForm());
            
        }
    }
}
