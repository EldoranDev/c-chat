﻿namespace C_Chat
{
    partial class ChatForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatForm));
            this.CopyStripe = new System.Windows.Forms.StatusStrip();
            this.CopyLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.DropDown = new System.Windows.Forms.ToolStripDropDownButton();
            this.JoinChat = new System.Windows.Forms.ToolStripMenuItem();
            this.AdminPanel = new System.Windows.Forms.ToolStripDropDownButton();
            this.kickToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToWrite = new System.Windows.Forms.TextBox();
            this.button_senden = new System.Windows.Forms.Button();
            this.WrittenText = new System.Windows.Forms.RichTextBox();
            this.Checker = new System.Windows.Forms.Timer(this.components);
            this.Counter = new System.Windows.Forms.Timer(this.components);
            this.CopyStripe.SuspendLayout();
            this.SuspendLayout();
            // 
            // CopyStripe
            // 
            this.CopyStripe.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyLabel,
            this.DropDown,
            this.AdminPanel});
            this.CopyStripe.Location = new System.Drawing.Point(0, 392);
            this.CopyStripe.Name = "CopyStripe";
            this.CopyStripe.Size = new System.Drawing.Size(389, 22);
            this.CopyStripe.TabIndex = 0;
            this.CopyStripe.Text = "statusStrip1";
            // 
            // CopyLabel
            // 
            this.CopyLabel.Name = "CopyLabel";
            this.CopyLabel.Size = new System.Drawing.Size(94, 17);
            this.CopyLabel.Text = "© Eldoran 2011 |";
            // 
            // DropDown
            // 
            this.DropDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DropDown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.JoinChat});
            this.DropDown.Image = ((System.Drawing.Image)(resources.GetObject("DropDown.Image")));
            this.DropDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DropDown.Name = "DropDown";
            this.DropDown.Size = new System.Drawing.Size(45, 20);
            this.DropDown.Text = "Chat";
            // 
            // JoinChat
            // 
            this.JoinChat.Name = "JoinChat";
            this.JoinChat.Size = new System.Drawing.Size(123, 22);
            this.JoinChat.Text = "Join Chat";
            this.JoinChat.Click += new System.EventHandler(this.JoinChat_Click);
            // 
            // AdminPanel
            // 
            this.AdminPanel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.AdminPanel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kickToolStripMenuItem});
            this.AdminPanel.Enabled = false;
            this.AdminPanel.Image = ((System.Drawing.Image)(resources.GetObject("AdminPanel.Image")));
            this.AdminPanel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AdminPanel.Name = "AdminPanel";
            this.AdminPanel.Size = new System.Drawing.Size(85, 20);
            this.AdminPanel.Text = "AdminPanel";
            this.AdminPanel.Visible = false;
            // 
            // kickToolStripMenuItem
            // 
            this.kickToolStripMenuItem.Name = "kickToolStripMenuItem";
            this.kickToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.kickToolStripMenuItem.Text = "Kick";
            // 
            // ToWrite
            // 
            this.ToWrite.Location = new System.Drawing.Point(12, 369);
            this.ToWrite.Name = "ToWrite";
            this.ToWrite.ReadOnly = true;
            this.ToWrite.Size = new System.Drawing.Size(276, 20);
            this.ToWrite.TabIndex = 2;
            this.ToWrite.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FToWrite);
            // 
            // button_senden
            // 
            this.button_senden.Enabled = false;
            this.button_senden.Location = new System.Drawing.Point(293, 369);
            this.button_senden.Name = "button_senden";
            this.button_senden.Size = new System.Drawing.Size(83, 19);
            this.button_senden.TabIndex = 3;
            this.button_senden.Text = "Senden";
            this.button_senden.UseVisualStyleBackColor = true;
            this.button_senden.Click += new System.EventHandler(this.FToWrite);
            // 
            // WrittenText
            // 
            this.WrittenText.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.WrittenText.Location = new System.Drawing.Point(12, 12);
            this.WrittenText.Name = "WrittenText";
            this.WrittenText.ReadOnly = true;
            this.WrittenText.Size = new System.Drawing.Size(365, 351);
            this.WrittenText.TabIndex = 1;
            this.WrittenText.Text = "";
            // 
            // Checker
            // 
            this.Checker.Enabled = true;
            this.Checker.Tick += new System.EventHandler(this.Checker_Tick);
            // 
            // Counter
            // 
            this.Counter.Enabled = true;
            this.Counter.Interval = 1;
            this.Counter.Tick += new System.EventHandler(this.Counter_Tick);
            // 
            // ChatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 414);
            this.Controls.Add(this.button_senden);
            this.Controls.Add(this.ToWrite);
            this.Controls.Add(this.WrittenText);
            this.Controls.Add(this.CopyStripe);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChatForm";
            this.Text = "C# Chat ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChatForm_FormClosing);
            this.CopyStripe.ResumeLayout(false);
            this.CopyStripe.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip CopyStripe;
        private System.Windows.Forms.ToolStripStatusLabel CopyLabel;
        private System.Windows.Forms.TextBox ToWrite;
        private System.Windows.Forms.Button button_senden;
        private System.Windows.Forms.ToolStripDropDownButton DropDown;
        private System.Windows.Forms.ToolStripMenuItem JoinChat;
        private System.Windows.Forms.RichTextBox WrittenText;
        private System.Windows.Forms.ToolStripDropDownButton AdminPanel;
        private System.Windows.Forms.ToolStripMenuItem kickToolStripMenuItem;
        private System.Windows.Forms.Timer Checker;
        private System.Windows.Forms.Timer Counter;
    }
}

