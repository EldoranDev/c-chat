﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C_Chat.Configs
{
    class User
    {
        public static string EnvName = "";
        public static OperatingSystem EnvOS = null;
        public static int LanguageID;
        public static string Name = ""; //Name of Client
        public static int PostCount; //helping Variable
        public static int PostPerMinMax = 15; //In Posts
        public static int MuteTime = 30; //In Seconds
    }
}
