﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat.Parser
{
    class Help
    {
        static public string[] Reset(string[] part)
        {
            for (int i = 0; i < part.Length; i++)
            {
                part[i] = "";
            }

            return part;
        }

        static public string PartToString(string[] part)
        {
            string PString = "";
            string temp = "";
            for (int i = 0; i < part.Length; i++)
            {
                temp += part[0];
                temp += "|";
            }
            for (int i = 0; i < temp.Length; i++)
            {
                PString += PString[i];
            }

            return PString;
        }
        static public Byte[] GetBytes(string Text)
        {
            return ASCIIEncoding.ASCII.GetBytes(Text);
        }

        static public Byte[] GetVersionByte()
        {
            Byte[] Version = ASCIIEncoding.ASCII.GetBytes(Configs.Version);
            return Version;
        }
    }
}
