using System;
using System.Net;
using System.Net.Sockets;

namespace Chat
{
	public class Broadcast
	{
		
		public void BoradCastSend(byte[] data, int Port)
		{
			this.BroadCastSend(data, IPAddress.Broadcast, Port);
		}
		
		public void BroadCastSend(byte[] data,IPAddress ip, int Port)
		{
			Socket bcSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			IPEndPoint iep1 = new IPEndPoint(ip, Port);
			bcSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
			bcSocket.SendTo(data, iep1);

			
			bcSocket.Close();
		}
	}
}

