﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat
{
    class MessageParser
    {
        public void Parse(String[] InString) // The Main Parse Element
        {
            if (InString.Length > 0)
            {
                switch (InString[0])
                {
                    case "pn":
                        //Handler for Personal Messages
                        ParsePN(InString);
                        break;
                    case "cm":
                        //Handler for Commands
                        ParseCom(InString);
                        break;
                    case "bc":
                        //Handler for Normal Broadcast Messages
                        ParseBC(InString);
                        break;
                }
            }
        }

        private void ParsePN(String[] PNString) // The Funktion to Parse PN's
        {

        }

        private void ParseCom(String[] COMString) // The Funktion to Parse Commands
        {

        }

        private void ParseBC(String[] BCString) // The Funktion to Parse Normal Broadcasts
        {

        }
    }
}
