using System;
namespace Chat
{
	public class Writer
	{
		ConsoleColor Default;
		
		public Writer()
		{
			Default = Console.ForegroundColor;
		}
		
		public void W(string Text, ConsoleColor C)
		{
			Console.ForegroundColor = C;
			Console.Write(Text);
			Console.ForegroundColor = Default;
		}
	}
}

