using System;
using System.Text;

namespace Chat
{
	public class MessageParserAlt
	{
		/* Rewrite of this File *
         * New Packet system to implement + Anti Sniffing in Broadcast.cs (Neues Byte Paket hinzufügen);
         * Neues Sende Pakt : <typ>|<hidden>|<user>|<to>|<message>
         * Byte : <VersionsByte> + <MessagePaketByte>
         */
        public static string[] UserMessage(string StringData)
		{
			string[] Text;
			Text = StringData.Split('|');
			return Text;
		}
		
		public static string Setname(string StringData, Writer writer, Broadcast Sender)
		{
			string Name = "";
			string[] temp = StringData.Split(' ');
			if (temp.Length > 1) {
				Name = temp[1];
			}
            
			return Name;
		}

        public static void WelcomeMessage(Broadcast caster)
        {
            ASCIIEncoding enc = new ASCIIEncoding();
            caster.BoradCastSend(enc.GetBytes("!~"+Configs.Name+" Joint Chat~!\n"),13337);
        }
	}
}

