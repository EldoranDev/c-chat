﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chat.Parser;

namespace Chat
{
    /*  => new Message Parser File for the New Sending System
     *  => Used Syntax: 
     *  =>          <typ>|<hidden>|<user>|<color>|<to>|<message>
     */
    class MessageSender
    {
        string[] part = new string[6];

        public void Senden(Broadcast caster){
            int x = 0;
            Byte[] VersionBytes = Help.GetVersionByte();
            Byte[] MessageBytes = Help.GetBytes(Help.PartToString(part));
            Byte[] EndBytes = new Byte[VersionBytes.Length + MessageBytes.Length];
            for (int i = 0; i < VersionBytes.Length; i++)
            {
                EndBytes[x] = VersionBytes[i];
                x++;
            }
            for (int i = 0; i < MessageBytes.Length; i++)
            {
                EndBytes[x] = MessageBytes[i];
                x++;
            }
            caster.BoradCastSend(EndBytes, Configs.Port);
        }
    }
}
