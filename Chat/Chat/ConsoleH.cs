﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat
{
    class ConsoleH
    {
        public static void RemoveLine()
        {
            Console.CursorTop--;
            Console.Write("\r");
            for (int i = 0; i < Console.BufferWidth; i++)
            {
                Console.Write(" ");
            }
            Console.CursorTop--;
            Console.Write("\r");
        }
    }
}
