using System;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Chat
{
	class MainClass
	{
        [STAThread]	
		public static void Main (string[] args)
		{
			
            Writer writer = new Writer();
            MessageSender Parser = new MessageSender();
			ListenerOld listener = new ListenerOld();	
			ASCIIEncoding enc = new ASCIIEncoding();
			Broadcast Sender = new Broadcast();
			ThreadStart Tlistener = new ThreadStart(listener.listen);
			Thread TListerner = new Thread(Tlistener);
			TListerner.Start();

            Application.Run(new CForm.ChatForm());

			string toSend;
			string[] Command;
			string Message;
			
			while(true)
			{
				toSend = Console.ReadLine();
                ConsoleH.RemoveLine();
				Command = toSend.Split(' ');
                if (toSend.Length > 1 & Configs.Running)
                {


                    if (Configs.Name != "" )
                    {
                        if (toSend[0].Equals('?') & toSend[1].Equals('!'))
                        {
                            //Sendet die Message verdeckt �ber den Broadcast \\ M�glichkeit f�r PN's Remote Kill usw
                            Sender.BoradCastSend(enc.GetBytes(toSend), Configs.Port);
                        }
                        else if (Command[0].Equals("?setName"))
                        {
                            //Setzt den Namen
                            Configs.Name = MessageParserAlt.Setname(toSend, writer, Sender);
                            MessageParserAlt.WelcomeMessage(Sender);
                        }
                        else
                        {
                            //Sendet die Message �ber den Broadcast
                            Message = Configs.Name + "|" + toSend;
                            Sender.BoradCastSend(enc.GetBytes(Message), Configs.Port);
                        }
                    }
                    else if (Command[0].Equals("?setName"))
                    {
                        //Setzt den Namen
                        Configs.Name = MessageParserAlt.Setname(toSend, writer, Sender);
                        MessageParserAlt.WelcomeMessage(Sender);
                    }
                    else
                    {
                        //Error falls noch kein Name gesendet ist
                        writer.W("Bitte erst  einen namen Setzen (mit ?setname <name>)\n", ConsoleColor.Red);
                    }
                }
			}
		}
	}
}

