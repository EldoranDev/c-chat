using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Chat
{
    public class ListenerOld
    {
        public bool Running = true;

        public void listen()
        {
            byte[] data;
            int recv;
            string[] BCCommand;
            Writer Writer = new Writer();


            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint iep = new IPEndPoint(IPAddress.Any, Configs.Port);
            EndPoint ep = (EndPoint)iep;
            sock.Bind(iep);
            Console.WriteLine("Willkommen zum C#at");

            while (Running)
            {
                data = new byte[1024];
                recv = sock.ReceiveFrom(data, ref ep);
                string StringData = Encoding.ASCII.GetString(data, 0, recv);

                if (StringData[0].Equals('?'))
                {
                    BCCommand = StringData.Split(' ');
                    if (BCCommand.Length > 2)
                    {
                        switch (BCCommand[1])
                        {
                            case "kill":
                                if (BCCommand.Length == 4)
                                {
                                    if (BCCommand[2].Equals(Configs.Name) & BCCommand[3].Equals(Configs.PW))
                                    {
                                        this.Running = false;
                                        Configs.Running = false;
                                        Writer.W("Du wurdest vom Chat getrennt", ConsoleColor.Red);
                                    }
                                }
                                break;
                        }
                    }
                }
                else if (StringData[0].Equals('!'))
                {
                    Writer.W(StringData, ConsoleColor.DarkYellow);
                }
                else
                {
                    string[] Print = MessageParserAlt.UserMessage(StringData);
                    if (Print.Length > 1)
                    {
                        Writer.W(Print[0], ConsoleColor.Green);
                        Console.Write(": ");
                        Writer.W(Print[1] + "\n", ConsoleColor.White);
                    }
                }
            }
            sock.Close();
        }
    }
}

