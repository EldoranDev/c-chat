﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;

using System.Windows.Forms;

namespace Chat.CForm
{
    public class ChatForm : Form
    {
        private TextBox Input;
        private RichTextBox Outputs;
        private Broadcast caster = new Broadcast();

        private void InitializeComponent()
        {
            this.Input = new System.Windows.Forms.TextBox();
            this.Outputs = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Input
            // 
            this.Input.Location = new System.Drawing.Point(12, 322);
            this.Input.Name = "Input";
            this.Input.Size = new System.Drawing.Size(324, 20);
            this.Input.TabIndex = 0;
            this.Input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Input_KeyDown);
            // 
            // Outputs
            // 
            this.Outputs.Location = new System.Drawing.Point(13, 13);
            this.Outputs.Name = "Outputs";
            this.Outputs.ReadOnly = true;
            this.Outputs.Size = new System.Drawing.Size(323, 303);
            this.Outputs.TabIndex = 1;
            this.Outputs.Text = "";
            // 
            // ChatForm
            // 
            this.ClientSize = new System.Drawing.Size(348, 354);
            this.Controls.Add(this.Outputs);
            this.Controls.Add(this.Input);
            this.Name = "ChatForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public ChatForm()
        {
            InitializeComponent();
        }

        private void Input_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                TextBox Input = (TextBox)sender;
                string Message = Configs.Name + "|" + Input.Text;
                caster.BoradCastSend(System.Text.ASCIIEncoding.ASCII.GetBytes(Message),13337);
                Input.Text = "";
            }
        }
    }
}
